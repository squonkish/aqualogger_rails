Rails.application.routes.draw do
  root 'home#index'

  get 'add_data', to: 'home#add_data'
end
