class CreateWaterData < ActiveRecord::Migration[5.0]
  def change
    create_table :water_data do |t|
      t.decimal :temp, precision: 10, scale: 2
      t.decimal :humidity, precision: 10, scale: 2
      t.decimal :ph, precision: 10, scale: 2
      t.decimal :water_level, precision: 10, scale: 2
      t.decimal :water_temp, precision: 10, scale: 2
      t.decimal :d_o, precision: 10, scale: 2
      t.decimal :nitrate, precision: 10, scale: 2
      t.decimal :ammonium, precision: 10, scale: 2
      t.decimal :tds, precision: 10, scale: 2
      t.decimal :flow_rate, precision: 10, scale: 2

      t.timestamps
    end
  end
end
