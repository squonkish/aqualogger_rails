class HomeController < ApplicationController
  def index
    @data = WaterDatum.order(created_at: :desc).page(params[:page]).per(25)
  end

  def add_data
    water_data_params = params.permit :temp, :humidity, :ph, :water_level,
      :water_temp, :nitrate, :ammonium, :tds, :flow_rate
    water_data_params[:d_o] = params[:do]
    WaterDatum.create! water_data_params
    head :ok, content_type: "text/html"
  end

  def hour
  end

  def minute

  end
end
